FROM launcher.gcr.io/google/debian9 AS build

ENV HELM_VERSION=2.8.2
ENV HELM_URL=https://kubernetes-helm.storage.googleapis.com/helm-v${HELM_VERSION}-linux-amd64.tar.gz

RUN apt-get update \
    && apt-get install -y --no-install-recommends gettext wget
    
# Install Helm
RUN wget -q -O - ${HELM_URL} | tar zxf - \
    && mv linux-amd64/helm /usr/bin/ \
    && chmod +x /usr/bin/helm \
    && helm version --client

# Add the chart and initialize Helm
ADD gitlab /gitlab
RUN cp -r /gitlab /tmp/chart \
    && cd /tmp/chart \
    && helm init --client-only \
    && helm repo add gitlab https://charts.gitlab.io/ \
    && helm repo update \
    && helm dep update

# GZip and Tar the chart
RUN cd /tmp \
    && tar -czvf /tmp/gitlab.tar.gz chart

# Add our schema file
ADD schema.yaml /tmp/schema.yaml

# Provide registry prefix and tag for default values for images.
ARG REGISTRY
ARG TAG
RUN cat /tmp/schema.yaml \
    | env -i "REGISTRY=$REGISTRY" "TAG=$TAG" envsubst \
    > /tmp/schema.yaml.new \
    && mv /tmp/schema.yaml.new /tmp/schema.yaml


FROM gcr.io/cloud-marketplace-tools/k8s/deployer_helm
COPY --from=build /tmp/gitlab.tar.gz /data/chart/
COPY --from=build /tmp/schema.yaml /data/

# Add the open source license file
ADD deployer/source /source
