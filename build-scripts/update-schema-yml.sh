#!/usr/bin/env bash
# update-schema-yml
# Script to update the image tags of schema.yml based on the image tags
# of the rendered helm template.

set -e

SCHEMA_FILE=${SCHEMA_FILE:-schema.yaml}

function findImage() {
    grep -q "default: [\"]\{0,1\}\$REGISTRY/$1" $SCHEMA_FILE
}

function replaceTagOnImage() {
    IMAGE=$1
    TAG=$2
    pattern="s/[\"]\{0,1\}\$REGISTRY\/${IMAGE}:.*[\"]\{0,1\}$/\"\$REGISTRY\/${IMAGE}:${TAG}\"/"
    sed -i -e $pattern $SCHEMA_FILE
}

if [ ! -f $SCHEMA_FILE ]; then
  echo "Unable to find file: $SCHEMA_FILE"
fi

for image in $(build-scripts/list-helm-images.sh); do
    sourceImageName="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    sourceImageTag="${image//*:}"
    echo -n "Image: ${sourceImageName}"
    if ! findImage $sourceImageName ; then
        echo ' !! NOT FOUND'
    else
        echo ''
        replaceTagOnImage $sourceImageName $sourceImageTag
    fi
done
