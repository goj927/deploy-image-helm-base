#!/usr/bin/env bash

set -e

GCR_REGISTRY=${GCR_REGISTRY:-"gcr.io/top-chain-204115/gitlab"}

for image in $(build-scripts/list-helm-images.sh); do
    SOURCE_IMAGE_NAME="$(echo "${image##*/}" | cut -d':' -f1 | cut -d'@' -f1)"
    SOURCE_IMAGE_TAG="${image//*:}"
    MIRRORED_IMAGE="${GCR_REGISTRY}/${SOURCE_IMAGE_NAME}:${SOURCE_IMAGE_TAG}"

    docker pull "$image"
    docker tag "$image" "$MIRRORED_IMAGE"
    docker push "$MIRRORED_IMAGE"
done
