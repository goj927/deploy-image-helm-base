function major_version_number() {
  echo "$1" | cut -d"." -f 1
}