# Deploying GitLab to GKE via Google Cloud Marketplace

[GitLab](https://about.gitlab.com) is a single application for the complete DevOps lifecycle from project planning and source code management to CI/CD and monitoring.

The [Google Cloud Marketplace](https://cloud.google.com/launcher/) is a easy way to deploy apps like GitLab to a [Google Kubernetes Engine](https://cloud.google.com/kubernetes-engine/) cluster, with just a few clicks.

> **Note:** For production deployments, we recommend using the [`gitlab` Helm chart](https://docs.gitlab.com/ee/install/kubernetes/gitlab_chart.html) and configuring [external PostgreSQL, Redis, and object storage services](https://gitlab.com/charts/gitlab/tree/master/doc/advanced).

# Installation

## Quick install with Google Cloud Marketplace

Deploy GitLab to Google Kubernetes Engine using Google Cloud Marketplace, by following the [on-screen instructions](https://console.cloud.google.com/marketplace/details/gitlab-public/gitlab).

## Command line instructions

### Prerequisites

#### Set up command-line tools

You'll need the following tools in your development environment:
- [gcloud](https://cloud.google.com/sdk/gcloud/)
- [kubectl](https://kubernetes.io/docs/reference/kubectl/overview/)
- [Helm](https://github.com/kubernetes/helm/blob/master/docs/install.md)
- [docker](https://docs.docker.com/install/)
- [git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

Configure `gcloud` as a Docker credential helper:

```shell
gcloud auth configure-docker
```

#### Create a Google Kubernetes Engine cluster

Create a new cluster from the command-line.

```shell
export CLUSTER=marketplace-cluster
export ZONE=us-west1-a

gcloud container clusters create "$CLUSTER" --zone "$ZONE"
```

Configure `kubectl` to talk to the new cluster.

```shell
gcloud container clusters get-credentials "$CLUSTER" --zone "$ZONE"
```

#### Clone this repo

Clone this repo and the associated tools repo.

```shell
git clone --recurse-submodules https://gitlab.com/charts/deploy-image-helm-base.git
gcloud source repos clone google-marketplace-k8s-app-tools --project=k8s-marketplace-eap
```

#### Install the Application resource definition

Do a one-time setup for your cluster to understand Application resources.

```shell
kubectl apply -f google-marketplace-k8s-app-tools/crd/*
```

The Application resource is defined by the
[Kubernetes SIG-apps](https://github.com/kubernetes/community/tree/master/sig-apps)
community. The source code can be found on
[github.com/kubernetes-sigs/application](https://github.com/kubernetes-sigs/application).

#### Configure the app

Open and edit `deploy-image-helm-base/gitlab/values.yaml` to customize the settings and desired container images. Additional information on the available settings is available in the `deploy-image-helm-base/gitlab/docs/` folder.

#### Expand the manifest template and apply to Kubernetes

```shell
helm template gitlab --set APP_INSTANCE_NAME=$APP_INSTANCE_NAME,NAMESPACE=$NAMESPACE > expanded.yaml
kubectl apply -f expanded.yaml
```

### Setting up DNS

Retrieve the IP address GitLab is available at, note it may take a few minutes for the IP address to populate:

```shell
kubectl get \
--namespace <namespace> \
ing <name>-unicorn \
-o jsonpath='{.status.loadBalancer.ingress[0].ip}'
```

Then configure a DNS record for the domain you provided during installation, resolving to the IP address you retrieved above.

### Signing in

Browse to https://`gitlab.<yourdomain>`.

GitLab is provisioned with a randomly generated administrator password. To retrieve it: 

```shell
# specify the variables values matching your installation:
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default

kubectl get secret -n $NAMESPACE $APP_INSTANCE_NAME-gitlab-initial-root-password -ojsonpath={.data.password} | base64 --decode
```

# Administration of GitLab

GitLab offers a number of different options to customize the behavior to your needs. More information is available in our [administration documentation](https://docs.gitlab.com/ee/administration/index.html#configuring-gitlab).

## Configuring a valid TLS certificate

By default GitLab will utilize self-signed certificates. To utilize your own certificate:

```shell
# specify the variables values matching your installation:
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default

kubectl create secret tls -n $NAMESPACE $APP_INSTANCE_NAME-wildcard-tls --cert=<path/to-full-chain.crt> --key=<path/to.key> --dry-run -o json | kubectl apply -f -
```

## Update GitLab

GitLab is made up of multiple containers, each with their own images. These individual containers should be updated together, to ensure proper functionality. Database migrations also need to be run, to update the schema and any required data.

Because of this, the best way to perform an upgrade is to clone this repo: 

```shell
# specify the variables values matching your installation:
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default

git clone --recurse-submodules https://gitlab.com/charts/deploy-image-helm-base.git
```

Next, edit `gitlab/values.yaml` and be sure that any changes made during installation, or after, have been applied. Once complete, expand the Helm chart and apply it:

```shell
helm template gitlab --set APP_INSTANCE_NAME=$APP_INSTANCE_NAME,NAMESPACE=$NAMESPACE > expanded.yaml
kubectl apply -f expanded.yaml
```

## Backup and Restore

Detailed documentation on backup and restore is available [here](https://gitlab.com/charts/gitlab/tree/master/doc/backup-restore).

## Scaling

To make it easier to scale GitLab we include [horizontal pod autoscalers](https://kubernetes.io/docs/tasks/run-application/horizontal-pod-autoscale/), which add additional replicas as load increases.

By default, these are limited to single replica. To view the current scaling status:

```shell
# specify the variables values matching your installation:
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default

kubectl get hpa \
  --namespace $NAMESPACE
  --selector app.kubernetes.io/name=$APP_INSTANCE_NAME
```

You can then change the parameters by deleting the autoscaler and recreating it. For example, to change the minimum pods to 2 and maximum pods to 10 for `gitlab-unicorn`:

```shell
kubectl patch hpa -n $NAMESPACE $APP_INSTANCE_NAME-unicorn --patch '{"spec":{"maxReplicas":10}}'
kubectl patch hpa -n $NAMESPACE $APP_INSTANCE_NAME-unicorn --patch '{"spec":{"minReplicas":2}}'
```

# Uninstall the Application

## Using GKE UI

Navigate to `GKE > Applications` in GCP console. From the list of applications, click on the one that you wish to uninstall.

On the new screen, click on the `Delete` button located in the top menu. It will remove
the resources attached to this application.

## Using the command line

### Prepare the environment

Set your installation name and Kubernetes namespace:

```shell
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default
```

### Delete the resources

> **NOTE:** Please keep in mind that `kubectl` guarantees support for Kubernetes server in +/- 1 versions.
> It means that for instance if you have `kubectl` in version 1.10.&ast; and Kubernetes 1.8.&ast;,
> you may experience incompatibility issues, like not removing the StatefulSets with
> apiVersion of apps/v1beta2.

If you still have the expanded manifest file used for the installation, you can use it to delete the resources.
Run `kubectl` on expanded manifest file matching your installation:

```shell
kubectl delete -f $APP_INSTANCE_NAME_manifest.yaml --namespace $NAMESPACE
```

Otherwise, delete the resources by indication of types and a label:

```shell
kubectl delete configmap,ingress,hpa,pdb,deployment,job,statefulset,secret,service \
  --namespace $NAMESPACE \
  --selector app.kubernetes.io/name=$APP_INSTANCE_NAME
```

### Delete the persistent volumes of your installation

By design, removal of resources in Kubernetes does not remove the PersistentVolumeClaims that
were attached to their Pods. It protects your installations from mistakenly deleting important data.

If you wish to remove the PersistentVolumeClaims with their attached persistent disks, run the
following `kubectl` commands:

```shell
# specify the variables values matching your installation:
export APP_INSTANCE_NAME=gitlab-1
export NAMESPACE=default

kubectl delete persistentvolumeclaims \
  --namespace $NAMESPACE
  --selector app.kubernetes.io/name=$APP_INSTANCE_NAME
```